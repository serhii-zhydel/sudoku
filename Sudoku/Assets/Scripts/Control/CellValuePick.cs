using UnityEngine;
using UnityEngine.UI;

public class CellValuePick:MonoBehaviour
{
	public Button ButtonToSubscribe;

	public int ValueToPick;

	public CellValueEditor ValueEditor;

	private void OnValidate()
	{
		if (ButtonToSubscribe == null)
			ButtonToSubscribe = GetComponentInChildren<Button>();

		if (ValueEditor == null)
			ValueEditor = FindObjectOfType<CellValueEditor>();
	}

	private void OnEnable()
	{
		ButtonToSubscribe.onClick.AddListener(OnButtonPressed);
	}

	private void OnDisable()
	{
		ButtonToSubscribe.onClick.RemoveListener(OnButtonPressed);
	}

	private void OnButtonPressed()
	{
		ValueEditor.FinishEditCellValue(ValueToPick);
	}
}