using System;
using UnityEngine;
using UnityEngine.Events;


[Serializable]
public class CellEvent : UnityEvent<Cell> { }

public class CellValueEditor : MonoBehaviour
{
	public CellEvent CellValueEditing         = new CellEvent();
	public CellEvent EditingCellValueCanceled = new CellEvent();
	public CellEvent CellValueEdited          = new CellEvent();

	private Cell _editedCell;

	public void StartEditCellValue(Cell cell)
	{
		if (cell.Equals(_editedCell))
			return;

		if (_editedCell != null)
			CancelEditCellValue();

		if (cell.IsAllowedToChangeValue)
		{
			_editedCell = cell;
			CellValueEditing.Invoke(cell);
		}
	}

	public void CancelEditCellValue()
	{
		EditingCellValueCanceled.Invoke(_editedCell);
		_editedCell = null;
	}

	public void FinishEditCellValue(int pickedValue)
	{
		if (_editedCell == null)
			return;

		_editedCell.Value = pickedValue;
		CellValueEdited.Invoke(_editedCell);
		_editedCell = null;
	}
}