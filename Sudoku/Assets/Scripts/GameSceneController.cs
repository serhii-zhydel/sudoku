using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameSceneController : MonoBehaviour
{
	public GameObject WinPanel;
	public Button NextPuzzleButtonInWinPanel;


	public Button ResetPuzzleButton;
	public Toggle ShowHintToggle;
	public Button CheckPuzzleButton;
	public Toggle ShowSolutionToggle;
	public Button NextPuzzleButton;

	public int                LevelNumber = 0;
	public SudokuLevelManager LevelsManager;

	public GridView GridView;

	private Grid _gridModel;
	private Grid _solvedGridModel;

	private bool _isShowingSolution = false;

	private void Awake()
	{
		int[][] rawLevelData = LevelsManager.GetLevel(LevelNumber);

		_gridModel = new Grid(3);

		LoadRawLevelToGrid(rawLevelData);
		GridView.Init(_gridModel);

		ResetPuzzleButton.onClick.AddListener(ResetGrid);
		ShowHintToggle.onValueChanged.AddListener(GridView.ShowHint);
		CheckPuzzleButton.onClick.AddListener(CheckSolution);
		ShowSolutionToggle.onValueChanged.AddListener(ShowSolution);
		NextPuzzleButton.onClick.AddListener(NextPuzzle);
		NextPuzzleButtonInWinPanel.onClick.AddListener(NextPuzzle);

		TrackPlayerWin();
	}

	private void TrackPlayerWin()
	{
		foreach (var cell in _gridModel.Cells.SelectMany(row=>row))
		{
			cell.ValueChanged += cell1 =>
			{
				if (!GetCellsWithWrongValue().Any())
					WinPanel.SetActive(true);
			};
		}
	}

	private void CheckSolution()
	{
		if(!_isShowingSolution)
		GridView.ShowWrongCells(GetCellsWithWrongValue());
	}

	private IEnumerable<Cell> GetCellsWithWrongValue()
	{
		var currentValues = _gridModel.Cells.SelectMany(row => row).ToArray();
		var rightValues   = _solvedGridModel.Cells.SelectMany(row => row).ToArray();

		for (int i = 0; i < currentValues.Length; i++)
		{
			if (currentValues[i].Value != rightValues[i].Value)
				yield return currentValues[i];
		}
	}

	private void NextPuzzle()
	{
		WinPanel.SetActive(false);

		ShowSolutionToggle.onValueChanged.RemoveListener(ShowSolution);
		ShowSolutionToggle.isOn = false;

		GridView.Reset();

		LevelNumber = (int) Mathf.Repeat(++LevelNumber, 3);
		int[][] nextLevelData = LevelsManager.GetLevel(LevelNumber);
		LoadRawLevelToGrid(nextLevelData);
		GridView.Init(_gridModel);
		ShowSolutionToggle.onValueChanged.AddListener(ShowSolution);
	}

	private void ResetGrid()
	{
		_gridModel.AutoCalculatePossibleValuesAfterCellChanged = false;

		foreach (var cell in _gridModel.Cells.SelectMany(row => row))
		{
			if (cell.IsAllowedToChangeValue)
				cell.Value = 0;
		}

		_gridModel.CalculatePossibleValues();
		_gridModel.AutoCalculatePossibleValuesAfterCellChanged = true;
	}

	private void LoadRawLevelToGrid(int[][] rawLevelData)
	{
		_gridModel.AutoCalculatePossibleValuesAfterCellChanged = false;

		foreach (var cell in _gridModel.Cells.SelectMany(row => row))
		{
			cell.IsAllowedToChangeValue = true;
			cell.Value                  = 0;
		}

		_gridModel.Init(rawLevelData);
		_gridModel.CalculatePossibleValues();

		CalculateSolution();

		_gridModel.AutoCalculatePossibleValuesAfterCellChanged = true;
	}

	private void CalculateSolution()
	{
		_solvedGridModel = SudokuSolver.SolveGrid(_gridModel);
		if (_solvedGridModel != null)
			foreach (var cell in _solvedGridModel.Cells.SelectMany(row => row))
				cell.IsAllowedToChangeValue = false;
		else
			Debug.Log($"No solutions for this sudoku", this);
	}

	private void ShowSolution(bool value)
	{
		if (value)
		{
			GridView.Reset();
			GridView.Init(_solvedGridModel);
			_isShowingSolution = true;
		}
		else
		{
			GridView.Reset();
			GridView.Init(_gridModel);
			_isShowingSolution = false;
		}
	}
}