using System;
using System.Linq;
using Newtonsoft.Json;

public class SudokuLevels
{
	public int[][] Grid_1;
	public int[][] Grid_2;
	public int[][] Grid_3;

	public int[][] GetGrid(int index)
	{
		switch (index)
		{
			case 0:
				return Grid_1;

			case 1:
				return Grid_2;

			case 2:
				return Grid_3;

			default:
				throw new ArgumentException(nameof(index));
		}
	}

	public static SudokuLevels CreateFromJSON(string jsonString)
	{
		//it says https://stackoverflow.com/questions/16359628/json-net-under-unity3d-for-ios)
		//that this version of Json.Net is compatible with IOS and WebGl.
		return JsonConvert.DeserializeObject<SudokuLevels>(jsonString);
	}
}