using System.Linq;
using UnityEngine;

public class SudokuLevelManager : ScriptableObject
{
	public  TextAsset    JsonWithGrids;
	private SudokuLevels _parsedLevels;

	public int [][] GetLevel(int index)
	{
		if (_parsedLevels == null)
			_parsedLevels = SudokuLevels.CreateFromJSON(JsonWithGrids.text);

		return _parsedLevels.GetGrid(index);
	}
}