using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GridView : MonoBehaviour
{
	public int         BaseNumber = 3;
	public BlockView[] Blocks;


	private void OnValidate()
	{
		if (Blocks == null || Blocks.Length == 0)
			Blocks = GetComponentsInChildren<BlockView>();
	}

	public void Init(Grid grid)
	{
		if (grid.BaseNumber != BaseNumber)
			throw new ArgumentException($"Wrong BlockView BaseNumber: {BaseNumber}. Must be: {grid.BaseNumber}");

		StartCoroutine(DisplayGrid(grid));
	}

	public void Reset()
	{
		CellView[] cellViews = Blocks.SelectMany(cellView => cellView.Cells).ToArray();
		foreach (var cellView in cellViews)
		{
			cellView.Reset();
		}
	}

	private IEnumerator DisplayGrid(Grid grid)
	{
		//Wait till canvas applies resizing.
		yield return new WaitForEndOfFrame();

		CellView[] cellViews = Blocks.SelectMany(cellView => cellView.Cells).ToArray();

		OrderCellsViews(cellViews);
		BindCellsToCellViews(grid, cellViews);

		OrderBlockViews(Blocks);
		HighlightBlocks(Blocks);
	}

	public void ShowWrongCells(IEnumerable<Cell> cellsWithWrongValue)
	{
		CellView[] cellViews = Blocks.SelectMany(cellView => cellView.Cells).ToArray();
		foreach (var cellWithWrongValue in cellsWithWrongValue)
		{
			var accordingCellView = Array.Find(cellViews,
				(cellView) => ReferenceEquals(cellView.CellModel,cellWithWrongValue));

			accordingCellView.ShowWrongValueState();
		}
	}

	private void OrderCellsViews(CellView[] cellViews)
	{
		Array.Sort(cellViews, (cell1, cell2) =>
		{
			var cell1Pos = cell1.transform.position;
			var cell2Pos = cell2.transform.position;

			var cell1Num = cell1Pos.y * -100 + cell1Pos.x;
			var cell2Num = cell2Pos.y * -100 + cell2Pos.x;

			return cell1Num.CompareTo(cell2Num);
		});
	}

	private void BindCellsToCellViews(Grid grid, CellView[] cellViews)
	{
		for (int y = 0; y < grid.CellsInLine; y++)
		{
			for (int x = 0; x < grid.CellsInLine; x++)
			{
				var cellViewIndex = y * grid.CellsInLine + x;
				var gridModel     = grid.Cells[y][x];
				cellViews[cellViewIndex].Init(gridModel);
			}
		}
	}

	private void OrderBlockViews(BlockView[] blockViews)
	{
		Array.Sort(blockViews, (block1, block2) =>
		{
			var block1Pos = block1.transform.position;
			var block2Pos = block2.transform.position;

			var block1Num = block1Pos.y * -100 + block1Pos.x;
			var block2Num = block2Pos.y * -100 + block2Pos.x;

			return block1Num.CompareTo(block2Num);
		});
	}

	private void HighlightBlocks(BlockView[] blockViews)
	{
		for (int i = 0; i < blockViews.Length; i++)
		{
			if (i % 2 == 0)
				blockViews[i].ShowHighlight();
		}
	}

	public void ShowHint(bool value)
	{
		if (value)
			foreach (var cellView in Blocks.SelectMany(block => block.Cells))
			{
				cellView.ShowPossibleValues();
				cellView.DisplayPossibleValues = true;
			}
		else
			foreach (var cellView in Blocks.SelectMany(block => block.Cells))
			{
				cellView.HidePossibleValues();
				cellView.DisplayPossibleValues = false;
			}
	}
}