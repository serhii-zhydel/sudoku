using UnityEngine;

public class BlockView : MonoBehaviour
{
	public int        BaseNumber = 3;
	public CellView[] Cells;

	private void OnValidate()
	{
		if (Cells == null || Cells.Length == 0)
			Cells = GetComponentsInChildren<CellView>();
	}

	[ContextMenu(nameof(ShowHighlight))]
	public void ShowHighlight()
	{
		foreach (var cellView in Cells)
		{
			cellView.Highlight(true);
		}
	}

	[ContextMenu(nameof(HideHighlight))]
	public void HideHighlight()
	{
		foreach (var cellView in Cells)
		{
			cellView.Highlight(false);
		}
	}
}