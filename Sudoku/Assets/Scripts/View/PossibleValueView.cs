using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PossibleValueView : MonoBehaviour
{
	public GameObject Panel;
	public TMP_Text[] PossibleValuesText;

	private void OnValidate()
	{
		if (PossibleValuesText == null || PossibleValuesText.Length == 0)
			PossibleValuesText = GetComponentsInChildren<TMP_Text>();
	}

	public void ShowPanel()
	{
		Panel.SetActive(true);
	}

	public void HidePanel()
	{
		Panel.SetActive(false);
	}

	public void ShowPossibleValues(List<int> possibleValues)
	{
		foreach (var text in PossibleValuesText)
		{
			text.text = String.Empty;
		}
		
		foreach (var value in possibleValues)
		{
			PossibleValuesText[value - 1].text = value.ToString();
		}
	}
}