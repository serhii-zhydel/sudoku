using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CellView : MonoBehaviour
{
	public TMP_Text Text;
	public Button   EditButton;

	//[NonSerialized]
	public Cell              CellModel;
	public Image             BGColor;
	public Image             WrongStateImage;
	public PossibleValueView PossibleValueView;

	public CellValueEditor ValueEditor;

	public bool IsHighlighted = false;

	public  bool DisplayPossibleValues      = true;
	private bool _isPossibleValuesDisplayed = false;

	public Color DefaultColor
	{
		get { return IsHighlighted ? ColorHighlighted : ColorNotHighlighted; }
	}

	public Color ColorNotHighlighted = Color.white;
	public Color ColorHighlighted    = Color.gray;

	public Color ColorEditing        = Color.yellow;
	public Color ColorForbidToChange = Color.grey;

	public float ShowWrongValueStateTime = 1;

	private void OnValidate()
	{
		if (Text == null)
			Text = GetComponentInChildren<TMP_Text>();

		if (EditButton == null)
			EditButton = GetComponentInChildren<Button>();

		if (ValueEditor == null)
			ValueEditor = FindObjectOfType<CellValueEditor>();

		if (BGColor == null)
			BGColor = transform.Find("CellBGWhite").GetComponent<Image>();

		if (PossibleValueView == null)
			PossibleValueView = GetComponentInChildren<PossibleValueView>();

		if (WrongStateImage == null)
			WrongStateImage = transform.Find("WrongValue").GetComponent<Image>();
	}

	public void Highlight(bool value)
	{
		if (value && !CellModel.IsAllowedToChangeValue) //don't change color in black cells
			return;

		IsHighlighted = value;
		BGColor.color = DefaultColor;
	}

	public void Init(Cell cell)
	{
		BGColor.color = DefaultColor;

		CellModel         =  cell;
		cell.ValueChanged += SetValue;
		SetValue(cell);

		if (cell.IsAllowedToChangeValue)
			EditButton.onClick.AddListener(EditValue);
		else
			BGColor.color = ColorForbidToChange;

		if (DisplayPossibleValues)
		{
			ShowPossibleValues();
		}
	}

	public void Reset()
	{
		BGColor.color          =  DefaultColor;
		CellModel.ValueChanged -= SetValue;
		Text.text              =  String.Empty;

		EditButton.onClick.RemoveAllListeners();

		if (_isPossibleValuesDisplayed)
			HidePossibleValues();

		StopCoroutine(nameof(WrongValueShowing));
		var calculatedColor = WrongStateImage.color;
		calculatedColor.a     = 0;
		WrongStateImage.color = calculatedColor;
	}

	[ContextMenu(nameof(ShowPossibleValues))]
	public void ShowPossibleValues()
	{
		if (CellModel.IsAllowedToChangeValue && !_isPossibleValuesDisplayed)
		{
			PossibleValueView.ShowPanel();
			PossibleValueView.ShowPossibleValues(CellModel.PossibleValues);
			CellModel.PossibleValuesChanged += PossibleValueView.ShowPossibleValues;
			_isPossibleValuesDisplayed      =  true;
		}
	}

	[ContextMenu(nameof(HidePossibleValues))]
	public void HidePossibleValues()
	{
		if (_isPossibleValuesDisplayed)
		{
			PossibleValueView.HidePanel();
			CellModel.PossibleValuesChanged -= PossibleValueView.ShowPossibleValues;
			_isPossibleValuesDisplayed      =  false;
		}
	}

	private void OnDestroy()
	{
		EditButton.onClick.RemoveAllListeners();
	}

	private void SetValue(Cell cell)
	{
		Text.text = (cell.Value == 0) ? String.Empty : cell.Value.ToString();
	}

	private void UpdatePossibleValues(List<int> possibleValues) { }

	private void EditValue()
	{
		BGColor.color = ColorEditing;
		ValueEditor.StartEditCellValue(CellModel);

		ValueEditor.EditingCellValueCanceled.AddListener(FinishEditingValue);
		ValueEditor.CellValueEdited.AddListener(FinishEditingValue);
	}

	private void FinishEditingValue(Cell model)
	{
		ValueEditor.EditingCellValueCanceled.RemoveListener(FinishEditingValue);
		ValueEditor.CellValueEdited.RemoveListener(FinishEditingValue);

		BGColor.color = DefaultColor;
	}

	public void ShowWrongValueState()
	{
		StopCoroutine(nameof(WrongValueShowing));
		StartCoroutine(nameof(WrongValueShowing));
	}

	private IEnumerator WrongValueShowing()
	{
		var calculatedColor = WrongStateImage.color;
		calculatedColor.a     = 1;
		WrongStateImage.color = calculatedColor;

		float elapsedTime     = 0.0f;
		while (elapsedTime < ShowWrongValueStateTime)
		{
			yield return null;
			elapsedTime           += Time.deltaTime;
			calculatedColor.a     =  1.0f - Mathf.Clamp01(elapsedTime / ShowWrongValueStateTime);
			WrongStateImage.color =  calculatedColor;
		}
	}
}