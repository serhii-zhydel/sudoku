using System.Linq;

public static class SudokuSolver
{
	public static Grid SolveGrid(Grid grid)
	{
		var gridCopy = (Grid)grid.Clone();
		gridCopy.CalculatePossibleValues();
		gridCopy.AutoCalculatePossibleValuesAfterCellChanged = true;

		FillCellsWithOnePossibleValue(gridCopy);

		if (HasCellWithNoPossibleValues(gridCopy))
			return null;

		if (AllCellsHaveValue(gridCopy))
			return gridCopy;

		var firstCellWithNoValue = gridCopy.Cells.SelectMany(row => row).First(cell => cell.Value == 0);

		foreach (var possibleValue in firstCellWithNoValue.PossibleValues.ToArray())
		{
			var newBranch = new SolutionBranch(gridCopy, firstCellWithNoValue);
			var solution  = newBranch.TryFindSolutionWithCellValue(possibleValue);
			if (solution != null)
				return solution;
		}

		return null;
	}

	public static bool AllCellsHaveValue(Grid grid)
	{
		return grid.Cells.SelectMany(row=>row).All(cell => cell.Value != 0);
	}


	public static void FillCellsWithOnePossibleValue(Grid grid)
	{
		//look for cell that could have only 1 possible value
		var cellsWithOnlyOnePossibleVariant =
			grid.Cells.SelectMany(row => row).Where(cell => cell.PossibleValues.Count == 1).ToArray();

		if (cellsWithOnlyOnePossibleVariant.Length > 0)
		{
			do
			{
				//set that cells to their possible values
				foreach (var cellWithOnlyOnePossibleVariant in cellsWithOnlyOnePossibleVariant)
					cellWithOnlyOnePossibleVariant.Value = cellWithOnlyOnePossibleVariant.PossibleValues[0];

				//maybe after changing there appeared new cell with only 1 possible value
				cellsWithOnlyOnePossibleVariant =
					grid.Cells.SelectMany(row => row).Where(cell => cell.PossibleValues.Count == 1).ToArray();
			}
			while (cellsWithOnlyOnePossibleVariant.Length > 0);
		}
	}

	public static bool HasCellWithNoPossibleValues(Grid grid)
	{
		return grid.Cells.SelectMany(row => row).Any(cell => cell.Value == 0 && cell.PossibleValues.Count == 0);
	}
}

public class SolutionBranch
{
	public Grid Snapshot;
	public Cell CellToApprove;

	public SolutionBranch(Grid gridToTest, Cell cellToApprove)
	{
		Snapshot      = (Grid) gridToTest.Clone();
		CellToApprove = cellToApprove;
		Snapshot.AutoCalculatePossibleValuesAfterCellChanged = true;
	}

	public Grid TryFindSolutionWithCellValue(int valueToTest)
	{
		CellToApprove.Value = valueToTest;

		SudokuSolver.FillCellsWithOnePossibleValue(Snapshot);

		if (SudokuSolver.HasCellWithNoPossibleValues(Snapshot))
			return null;

		if (SudokuSolver.AllCellsHaveValue(Snapshot))
			return Snapshot;

		var firstCellWithNoValue = Snapshot.Cells.SelectMany(row => row).First(cell => cell.Value == 0);

		foreach (var possibleValue in firstCellWithNoValue.PossibleValues.ToArray())
		{
			var newBranch = new SolutionBranch(Snapshot, firstCellWithNoValue);
			var solution  = newBranch.TryFindSolutionWithCellValue(possibleValue);
			if (solution != null)
				return solution;
		}

		return null;
	}
}