using System;
using System.Collections.Generic;
using System.Linq;

public class Grid : ICloneable
{
	public Cell[][]  Cells;
	public Block[][] Blocks;

	public readonly int BaseNumber   = 3;
	public readonly int CellsInLine  = 9;
	public readonly int BlocksInGrid = 9;

	private Cell[] _blockBuffer1;
	private Cell[] _rowBuffer1;
	private Cell[] _columnBuffer1;

	private Cell[] _blockBuffer2;
	private Cell[] _rowBuffer2;
	private Cell[] _columnBuffer2;

	private int[] _allPossibleDigits;

	private bool _autoCalculatePossibleValuesAfterCellChanged = false;
	public bool AutoCalculatePossibleValuesAfterCellChanged
	{
		get { return _autoCalculatePossibleValuesAfterCellChanged; }
		set
		{
			if (value)
			{
				if (!_autoCalculatePossibleValuesAfterCellChanged)
				{
					foreach (var cell in Cells.SelectMany(row => row))
						cell.ValueChanged += RecalculateValuesForRelatedCells;

					_autoCalculatePossibleValuesAfterCellChanged = true;
				}
			}
			else
			{
				if (_autoCalculatePossibleValuesAfterCellChanged)
				{
					foreach (var cell in Cells.SelectMany(row => row))
						cell.ValueChanged -= RecalculateValuesForRelatedCells;

					_autoCalculatePossibleValuesAfterCellChanged = false;
				}
			}
		}
	}

	public Grid() : this(3) { }

	public Grid(int baseNumber)
	{
		BaseNumber = baseNumber;

		CellsInLine  = baseNumber * baseNumber;
		BlocksInGrid = baseNumber * baseNumber;

		Cells = new Cell [CellsInLine][];

		for (int y = 0; y < CellsInLine; y++)
		{
			Cells[y] = new Cell[CellsInLine];

			for (int x = 0; x < CellsInLine; x++)
				Cells[y][x] = new Cell(x, y);
		}

		Blocks = new Block[baseNumber][];
		for (int yBlock = 0; yBlock < BaseNumber; yBlock++)
		{
			Blocks[yBlock] = new Block[baseNumber];
			for (int xBlock = 0; xBlock < BaseNumber; xBlock++)
			{
				Blocks[yBlock][xBlock] = new Block(this, xBlock, yBlock);
			}
		}

		_blockBuffer1  = new Cell[CellsInLine];
		_rowBuffer1    = new Cell[CellsInLine];
		_columnBuffer1 = new Cell[CellsInLine];

		_blockBuffer2  = new Cell[CellsInLine];
		_rowBuffer2    = new Cell[CellsInLine];
		_columnBuffer2 = new Cell[CellsInLine];

		//{1, 2, 3, 4, 5, 6, 7, 8, 9}
		_allPossibleDigits = Enumerable.Range(1, CellsInLine).ToArray();
	}

	private Grid(Grid gridToCopyFrom)
	{
		BaseNumber   = gridToCopyFrom.BaseNumber;
		CellsInLine  = gridToCopyFrom.CellsInLine;
		BlocksInGrid = gridToCopyFrom.BlocksInGrid;

		Cells = new Cell [CellsInLine][];
		for (int y = 0; y < CellsInLine; y++)
		{
			Cells[y] = new Cell[CellsInLine];

			for (int x = 0; x < CellsInLine; x++)
			{
				Cells[y][x]                        = new Cell(x, y);
				Cells[y][x].Value                  = gridToCopyFrom.Cells[y][x].Value;
				Cells[y][x].IsAllowedToChangeValue = gridToCopyFrom.Cells[y][x].IsAllowedToChangeValue;
				Cells[y][x].PossibleValues.AddRange(gridToCopyFrom.Cells[y][x].PossibleValues);
			}
		}

		Blocks = new Block[BaseNumber][];
		for (int yBlock = 0; yBlock < BaseNumber; yBlock++)
		{
			Blocks[yBlock] = new Block[BaseNumber];
			for (int xBlock = 0; xBlock < BaseNumber; xBlock++)
			{
				Blocks[yBlock][xBlock] = new Block(this, xBlock, yBlock);
			}
		}

		_blockBuffer1  = new Cell[CellsInLine];
		_rowBuffer1    = new Cell[CellsInLine];
		_columnBuffer1 = new Cell[CellsInLine];

		_blockBuffer2  = new Cell[CellsInLine];
		_rowBuffer2    = new Cell[CellsInLine];
		_columnBuffer2 = new Cell[CellsInLine];

		//{1, 2, 3, 4, 5, 6, 7, 8, 9}
		_allPossibleDigits = Enumerable.Range(1, CellsInLine).ToArray();
	}

	public void Init(int[][] blocks)
	{
		for (int blockNumber = 0; blockNumber < BlocksInGrid; blockNumber++)
		{
			int[] cellsInBlock = blocks[blockNumber];

			var yBlock = blockNumber / BaseNumber;
			var xBlock = blockNumber - yBlock * BaseNumber;

			var currentBlock = Blocks[yBlock][xBlock];

			int cellIndex = 0;
			foreach (var blockCell in currentBlock)
			{
				blockCell.Value                  = cellsInBlock[cellIndex];
				blockCell.IsAllowedToChangeValue = cellsInBlock[cellIndex] == 0;
				cellIndex++;
			}
		}
	}

	private IEnumerable<Cell> GetBlockByStartPos(int xBlockPos, int yBlockPos)
	{
		int blockYShift = yBlockPos * BaseNumber;
		int blockXShift = xBlockPos * BaseNumber;

		for (int yCell = 0; yCell < BaseNumber; yCell++)
		{
			for (int xCell = 0; xCell < BaseNumber; xCell++)
			{
				int cellPosY = blockYShift + yCell;
				int cellPosX = blockXShift + xCell;

				yield return Cells[cellPosY][cellPosX];
			}
		}
	}

	public void GetColumnNonAlloc(int xColumn, Cell[] buffer)
	{
		Array.Clear(buffer, 0, buffer.Length);

		for (int y = 0; y < CellsInLine; y++)
			buffer[y] = Cells[y][xColumn];
	}

	public void GetRowNonAlloc(int y, Cell[] buffer)
	{
		Array.Clear(buffer, 0, buffer.Length);

		Cells[y].CopyTo(buffer, 0);
	}

	public void CalculatePossibleValues()
	{
		for (int yCell = 0; yCell < CellsInLine; yCell++)
		{
			for (int xCell = 0; xCell < CellsInLine; xCell++)
			{
				var currentCell = Cells[yCell][xCell];
				RecalculateValuesForCell(currentCell);
			}
		}
	}

	private void RecalculateValuesForCell(Cell cell)
	{
		if (cell.Value != 0)
		{
			if (cell.PossibleValues.Count > 0)
				cell.SetPossibleValues(null);
			return;
		}

		var blockPosY = cell.Pos.Y / BaseNumber;
		var blockPosX = cell.Pos.X / BaseNumber;
		Blocks[blockPosY][blockPosX].GetCellsNotAlloc(_blockBuffer1);
		GetRowNonAlloc(cell.Pos.Y, _rowBuffer1);
		GetColumnNonAlloc(cell.Pos.X, _columnBuffer1);

		var rowDigits    = GetAllDigitsExceptCell(_rowBuffer1, cell).ToArray();
		var columnDigits = GetAllDigitsExceptCell(_columnBuffer1, cell).ToArray();
		var blockDigits  = GetAllDigitsExceptCell(_blockBuffer1, cell).ToArray();

		var allSetDigits   = rowDigits.Union(columnDigits).Union(blockDigits).ToArray();
		var possibleValues = _allPossibleDigits.Except(allSetDigits);
		cell.SetPossibleValues(possibleValues);
	}

	private IEnumerable<int> GetAllDigitsExceptCell(IEnumerable<Cell> cells, Cell exceptCell)
	{
		return cells.Where(cell => !ReferenceEquals(cell, exceptCell))
				.Where(cell => cell.Value != 0)
				.Select(cell => cell.Value);
	}

	public void RecalculateValuesForRelatedCells(Cell cell)
	{
		var blockPosY = cell.Pos.Y / BaseNumber;
		var blockPosX = cell.Pos.X / BaseNumber;
		Blocks[blockPosY][blockPosX].GetCellsNotAlloc(_blockBuffer2);
		GetRowNonAlloc(cell.Pos.Y, _rowBuffer2);
		GetColumnNonAlloc(cell.Pos.X, _columnBuffer2);

		var rowRelatedCells    = GetZeroCellsExceptCell(_rowBuffer2, cell);
		var columnRelatedCells = GetZeroCellsExceptCell(_columnBuffer2, cell);
		var blockRelatedCells  = GetZeroCellsExceptCell(_blockBuffer2, cell);

		var allRelatedCells = rowRelatedCells.Union(columnRelatedCells).Union(blockRelatedCells);

		foreach (var relatedCell in allRelatedCells)
			RecalculateValuesForCell(relatedCell);

		RecalculateValuesForCell(cell);
	}

	private IEnumerable<Cell> GetZeroCellsExceptCell(IEnumerable<Cell> cells, Cell exceptCell)
	{
		return cells.Where(cell => !ReferenceEquals(cell, exceptCell))
				.Where(cell => cell.Value == 0);
	}

	public object Clone()
	{
		return new Grid(this);
	}
}