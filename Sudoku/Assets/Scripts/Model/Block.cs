using System;
using System.Collections;
using System.Collections.Generic;

public class Block : IEnumerable<Cell>
{
	public readonly  Position FirstCellPosition;
	private readonly Grid     _grid;

	public Block(Grid grid, int xPosInGrid, int yPosInGrid)
	{
		_grid = grid;

		var yPos = yPosInGrid * _grid.BaseNumber;
		var xPos = xPosInGrid * _grid.BaseNumber;

		FirstCellPosition = new Position(xPos, yPos);
	}

	public Cell this[int x, int y]
	{
		get { return _grid.Cells[FirstCellPosition.Y + y][FirstCellPosition.X + x]; }
	}

	public IEnumerable<Cell> GetRow(int y)
	{
		var row = FirstCellPosition.Y + y;
		for (int i = FirstCellPosition.X; i < _grid.BaseNumber; i++)
		{
			yield return _grid.Cells[row][i];
		}
	}

	public IEnumerable<Cell> GetColumn(int xColumn)
	{
		var index = FirstCellPosition.X + xColumn;

		for (int y = FirstCellPosition.Y; y < _grid.BaseNumber; y++)
		{
			yield return _grid.Cells[y][index];
		}
	}

	public void GetCellsNotAlloc(Cell[] buffer)
	{
		Array.Clear(buffer, 0, buffer.Length);

		int index = 0;

		var yLast = FirstCellPosition.Y + _grid.BaseNumber;
		var xLast = FirstCellPosition.X + _grid.BaseNumber;

		for (int yCellPos = FirstCellPosition.Y; yCellPos < yLast; yCellPos++)
		{
			for (int xCellPos = FirstCellPosition.X; xCellPos < xLast; xCellPos++)
			{
				buffer[index] = _grid.Cells[yCellPos][xCellPos];
				index++;
			}
		}
	}

	public IEnumerator<Cell> GetEnumerator()
	{
		var yLast = FirstCellPosition.Y + _grid.BaseNumber;
		var xLast = FirstCellPosition.X + _grid.BaseNumber;

		for (int yCellPos = FirstCellPosition.Y; yCellPos < yLast; yCellPos++)
		{
			for (int xCellPos = FirstCellPosition.X; xCellPos < xLast; xCellPos++)
			{
				yield return _grid.Cells[yCellPos][xCellPos];
			}
		}
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}
}