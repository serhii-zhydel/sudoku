public struct Position
{
	public int X;
	public int Y;

	public Position(int x, int y)
	{
		X = x;
		Y = y;
	}

	public override string ToString()
	{
		return $"{nameof(X)}: {X}, {nameof(Y)}: {Y}";
	}

	public bool Equals(Position other)
	{
		return X == other.X && Y == other.Y;
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		return obj is Position && Equals((Position) obj);
	}

	public override int GetHashCode()
	{
		unchecked
		{
			return (X * 397) ^ Y;
		}
	}
}