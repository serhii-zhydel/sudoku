using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Cell : IEquatable<Cell>
{
	public Position Pos;

	//------------------IsAllowedToChangeValue----------------
	[SerializeField]
	private bool _isAllowedToChangeValue = true;

	public bool IsAllowedToChangeValue
	{
		get { return _isAllowedToChangeValue; }
		set { _isAllowedToChangeValue = value; }
	}


	//--------------------------Value------------------------
	private int              _value;
	public event Action<Cell> ValueChanged = cell => { };

	public int Value
	{
		get { return _value; }
		set
		{
			if (!_isAllowedToChangeValue)
				throw new MemberAccessException($"You cannot change value of cell: {this.ToString()}");

			_value = value;
			ValueChanged.Invoke(this);
		}
	}
	//--------------------------Value------------------------

	public event Action<List<int>> PossibleValuesChanged = value => { };

	public List<int> PossibleValues = new List<int>(9);

	//-------------------------------------------------------


	public Cell(int x, int y)
	{
		Pos = new Position(x, y);
	}

	public void InitValue(int value)
	{
		Value                   = value;
		_isAllowedToChangeValue = false;
	}

	public void SetPossibleValues(IEnumerable<int> values)
	{
		PossibleValues.Clear();

		if (values != null)
			PossibleValues.AddRange(values);

		PossibleValuesChanged.Invoke(PossibleValues);
	}

	public override string ToString()
	{
		return $"{nameof(Pos)}: {Pos}, {nameof(Value)}: {Value}";
	}

	public bool Equals(Cell other)
	{
		return other != null && Pos.Equals(other.Pos);
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != this.GetType()) return false;
		return Equals((Cell) obj);
	}

	public override int GetHashCode()
	{
		return Pos.GetHashCode();
	}
}